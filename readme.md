# desmos-tools
to share, use this link (or any gitlab link but not desmos links):
https://gitlab.com/garx/desmos-tools
(desmos links in this document change with every new version)

-------- Desmos Tools by garx (for work with sound) --------

(works on mobile as well)

note2freq: (what frequency certain note has)

https://www.desmos.com/calculator/9vl4okd9ro

fact2smt: (how much in semitones you must change the pitch of audio to change its speed by certain factor)

https://www.desmos.com/calculator/dbcldu07t9

smt2fact: (by what factor you must change the speed of audio to change its pitch by certain number of semitones)

https://www.desmos.com/calculator/7vtysoqlyl


you can calculate it without any interface:
(just replace variable names with numbers and type it into google or smth)

note2freq: freq = (440/64) * 2^(oct+(smt+3)/12) (where A5 ~ oct=5, smt=9, for example)

fact2smt: smt = 12 * log2(fact)

smt2fact: fact = 2^(smt/12)

powered by Desmos